<?php
class Persona_model extends MY_Model
{
    protected $table = 'Persona';

    public function __construct()
    {
        parent::__construct();
	}
	
	public function checkLogin($dpi, $password)
	{
		$existe = $this->db->from($this->table)->where('dpi',$dpi)->count_all_results();
		if($existe == 0){
			return 0;
		}
		$vivo = $this->db->from($this->table)->where('dpi',$dpi)->where('difunto',0)->count_all_results();
		if($vivo == 0){
			return 1;
		}
		$this->db->where('dpi', $dpi);
		$this->db->update($this->table, $password);
		return 2;
	}

	public function obtenerDatos($dpi)
	{
		$existe = $this->db->from($this->table)->where('dpi',$dpi)->count_all_results();
		if($existe == 0){
			return new stdClass();
		}
		return $this->db->select('idPersona, nombres, apellidos, fecha_defuncion')->from($this->table)->where('dpi',$dpi)->get()->row();
	}
}

/*

+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| ID          | int(11)      | NO   | PRI | NULL    | auto_increment |
| AREA_TITULO | varchar(150) | NO   |     | NULL    |                |
| AREA_CLAVE  | varchar(150) | YES  |     | NULL    |                |
| AREA_STATUS | tinyint(1)   | YES  |     | 1       |                |
+-------------+--------------+------+-----+---------+----------------+

 */
