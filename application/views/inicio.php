<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Acta de Defuncion</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="/assets/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="/assets/dist/css/adminlte.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

	<!-- jQuery -->
	<script src="/assets/plugins/jquery/jquery.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	
	<!-- Bootstrap 4 -->
	<script src="/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- daterangepicker -->
	<script src="/assets/plugins/moment/moment.min.js"></script>
	<script src="/assets/plugins/daterangepicker/daterangepicker.js"></script>
	<!-- Tempusdominus Bootstrap 4 -->
	<script src="/assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
	<!-- overlayScrollbars -->
	<script src="/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
	<!-- AdminLTE App -->
	<script src="/assets/dist/js/adminlte.js"></script>
	<!-- DataTables -->
	<script src="/assets/plugins/datatables/jquery.dataTables.js"></script>
	<script src="/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
</head>

<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="<?php echo base_url()?>"><b>Renap</b></a>
		</div>
		<!-- /.login-logo -->
		<div class="card">
		<?php $success      = $this->session->flashdata('success_msg'); ?>
		<?php if($success): ?>
			<div class="alert alert-success" role="alert"><?php echo $success; ?></div>
		<?php endif; ?>
		<?php $error      = $this->session->flashdata('error_msg'); ?>
		<?php if($error): ?>
			<div class="alert alert-error" role="alert"><?php echo $error; ?></div>
		<?php endif; ?>
			<div class="card-body login-card-body">
				<p class="login-box-msg">Crear Acta de Defunción</p>
				<input type="text" name="mensaje" id="mensaje" placeholder="Esperando..." readonly>

				<!--form action="setDefuncion" method="post"-->
					<div class="input-group mb-3">
						<input type="text" name="dpi" id="dpi" class="form-control" placeholder="DPI">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-person"></span>
							</div>
						</div>
					</div>
					<div class="input-group mb-3">
						<input name="fecha" id="fecha" class="form-control" placeholder="dd-mm-aaaa">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-calendar"></span>
							</div>
						</div>
					</div>

					<div class="input-group mb-3">
						<select class="form-control" id="grupo" name="grupo">
							<option value=''>- Seleccionar grupo -</option>
							<option value='35.184.41.20'>Grupo 1</option>
							<option value='35.239.54.7'>Grupo 2</option>
							<option value='35.184.97.83'>Grupo 3</option>
							<option value='35.193.113.191'>Grupo 4</option>
							<option value='35.232.98.125'>Grupo 5</option>
							<option value='35.232.40.193'>Grupo 6</option>
							<option value='35.211.247.121'>Grupo 7</option>
						</select>
					</div>

					<div class="row">
						<div class="col-4">
							<button onClick="myFunction()" class="btn btn-primary btn-block">Crear</button>
						</div>
						<!-- /.col -->
					</div>
				<!--/form-->
			</div>
			<!-- /.login-card-body -->
		</div>
	</div>
	<!-- /.login-box -->
	
	<!-- jQuery -->
	<script> 
		function myFunction() {
			var dpi_ = $('#dpi').val();
			var fecha_ = $('#fecha').val();
			var grupo = $('#grupo').val();
			var ini = 'http://';
			var ruta = ini.concat(grupo);
			alert(dpi);
			if(grupo == '35.193.113.191'){
				$.ajax({
					url: '/setDefuncion',
					type: "POST",
					dataType : "json",
					data:	{
									dpi: dpi_,
									fecha: fecha_
							},
					success:function(response){
						$("#mensaje").val(response.mensaje);
					}
				});
			}
			else{
				var ruta1 = '';
				var ruta2 = '';
				if(grupo == '35.239.54.7'){
					ruta1 = ruta.concat(':9006/post/comunicacionesb');
					ruta2 = ruta.concat(':9002/setDefuncion');
				}
				else{
					ruta1 = ruta.concat(':10000/post/comunicacionesb');
					ruta2 = ruta.concat(':9002/setDefuncion');
				}
				$.ajax({
					url: ruta1,
					type: "POST",
					dataType : "json",
					data:	{
								url: ruta2,
								tipo: 'post',
								parametros: {
									dpi: dpi_,
									fecha: fecha_
								}
							},
					success:function(response){
						$("#mensaje").val(response.mensaje);
					}
				});
			}
			
		}
	</script>

</body>

</html>
